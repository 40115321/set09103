from flask import Flask, g
import sqlite3
from contextlib import closing

DATABASE = 'flaskr.db'

TMDB_API_KEY = '&api_key=f2dc1ad6733d60e501537694e86049da'
TMDB_MOVIE_ID_URL = 'http://api.themoviedb.org/3/movie/id'
TMDB_MOVIE_SEARCH_URL = 'http://api.themoviedb.org/3/search/movie' 
TMDB_IMG_BASE_URL = "http://image.tmdb.org/t/p/w500"

app = Flask(__name__)
app.config.from_object(__name__)

app.secret_key = "A_REALLY_SECRET_KEY"

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

#Resets database
#init_db()

import src.views
