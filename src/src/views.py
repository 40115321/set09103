from src import app
from flask import Flask, request, redirect, render_template, session, \
g, url_for, abort, flash, jsonify

from src.classes import Movie

import urllib2
import json

@app.route('/')
def index():
	return render_template('base.html', page="index")

#Returns user profile via user id
@app.route('/user/<int:user_id>')
def user(user_id):
	return "User %s" %user_id

#Retruns list of catalogues created and stored in the database
@app.route('/catalogue')
def catalogue():
	cur = g.db.execute('select catid, title from catalogue')
	titles = [dict(catid=row[0], title=row[1]) for row in cur.fetchall()]
	return render_template('base.html', titles=titles, page="catalogues")

#View adds new catalogues to database
@app.route('/catalogue/add', methods=['GET', 'POST'])
def add_cat(): 
	if request.form['catTitle'] != "":
		g.db.execute('insert into catalogue (title) values (?)', [request.form['catTitle']])
		g.db.commit()
		flash('Success - You have added a new catalogue')
		return redirect(url_for('catalogue'))
	elif request.path == '/catalogue':
		return redirect(url_for('catalogue'))
	flash("Error - Catalogue title must not be empty")
	return redirect(url_for('index', _anchor='create'))

#Returns the user to catalogue from movie search results 
#Adds the movie to the catalogue that the user is editing
@app.route('/catalogue/<int:catid>/add/<int:movieid>')
def add_movie_to_catalogue(catid, movieid):
	g.db.execute('insert into CatalogueMovies (catid, movieid) values (?, ?)', (catid, movieid))
	g.db.commit()
	movie = get_movie_from_tmdb_by_id(movieid, False)
	add_movie_data_to_database(movie)
	flash('Success - You have added a movie to the catalogue')
	return redirect(url_for('catalogue_id', cat_id=catid))

#Returns the catologue view from id
#/catalogue/21 - links to the html containing details about that catalogue
@app.route('/catalogue/<int:cat_id>')
def catalogue_id(cat_id):
	if cat_id is not None:
		#Get the catalogue from id in url
		catalogue = get_catalogue_from_db(cat_id)
		#if this catalogue doesnt exist - 404
		if not catalogue:
			abort(404)
		else:
			#Catalogue exists - Get movies that are in the catalogue
			movies = get_movies_from_catalogue(cat_id)
			#If no movies are in the catalogue then show empty page
			if not movies:
				return render_template('base.html', catalogue=catalogue, movies=None, page='catalougeid')
			else:
				#Show page with movies passed over
				return render_template('base.html', catalogue=catalogue, movies=movies, page='catalougeid')
			abort(404)

#Update the title of the catalogue
@app.route('/catalogue/<int:cat_id>/update')
def update_catalogue_title(cat_id, title):
	cur = g.db.execute('update catalogue set title = ? where catid = ?', (title,cat_id,))
	g.db.commit()

@app.route('/catalogue/<int:cat_id>/genre/<genre>', methods=['GET', 'POST'])
def catalogue_genres(cat_id, genre=None):
	if cat_id is not None:
		#Get the catalogue from id in url
		catalogue = get_catalogue_from_db(cat_id)
		#if this catalogue doesnt exist - 404
		if not catalogue:
			abort(404)
		else:
			movies = get_movies_from_catalogue(cat_id)
			movieResults = []
			for movie in movies:
				if movie['moviegenre'] == genre:
					movieResults.append(movie)

			return render_template('base.html', movies=movieResults, catalogue=catalogue, page="catalougeid")
		abort(404)

#This is called when a form processed for release date to fix issues with getting request.form in the url_for method
@app.route('/catalogue/<int:cat_id>/release-date', methods=['GET', 'POST'])
def catalogue_release_date_fix(cat_id):
	if request.form['reldate'] != "":
		rel = request.form['reldate']
		catid = cat_id
		#Redirect to proper release date url with param from form
		return redirect(url_for('catalogue_release_date', cat_id=catid, releasedate=rel))

#Returns a list of movies of released in the year specified
@app.route('/catalogue/<int:cat_id>/release-date/<int:releasedate>', methods=['GET', 'POST'])
def catalogue_release_date(cat_id, releasedate):
	if cat_id is not None:
		#Get the catalogue from id in url
		catalogue = get_catalogue_from_db(cat_id)
		#if this catalogue doesnt exist - 404
		if not catalogue:
			abort(404)
		else:
			movies = get_movies_from_catalogue(cat_id)
			movieResults = []
			for movie in movies:
				spl = movie['moviereleasedate'][:4]
				if int(spl) == int(releasedate):
					movieResults.append(movie)

			return render_template('base.html', movies=movieResults, catalogue=catalogue, page="catalougeid")
		abort(404)

#This is called when a form processed for release date to fix issues with getting request.form in the url_for method
@app.route('/catalogue/<int:cat_id>/budget', methods=['GET', 'POST'])
def catalogue_budget_fix(cat_id):
	if request.form['budget'] != "":
		budget = request.form['budget']
		catid = cat_id
		#Redirect to proper release date url with param from form
		return redirect(url_for('catalogue_budget', cat_id=catid, budget=budget))

#Returns movies in a catalogue where the movie budget is >= the value specified by user
@app.route('/catalogue/<int:cat_id>/budget/<int:budget>', methods=['GET', 'POST'])
def catalogue_budget(cat_id, budget):
	if cat_id is not None:
		#Get the catalogue from id in url
		catalogue = get_catalogue_from_db(cat_id)
		#if this catalogue doesnt exist - 404
		if not catalogue:
			abort(404)
		else:
			movies = get_movies_from_catalogue(cat_id)
			movieResults = []
			for movie in movies:
				if int(movie['moviebudget']) >= budget:
					movieResults.append(movie)

			return render_template('base.html', movies=movieResults, catalogue=catalogue, page="catalougeid")
		abort(404)

#This is called when a form processed for release date to fix issues with getting request.form in the url_for method
@app.route('/catalogue/<int:cat_id>/runtime', methods=['GET', 'POST'])
def catalogue_runtime_fix(cat_id):
	if request.form['runtime'] != "":
		runtime = request.form['runtime']
		catid = cat_id
		#Redirect to proper release date url with param from form
		return redirect(url_for('catalogue_runtime', cat_id=catid, runtime=runtime))

#Returns a list of movies of released in the year specified
@app.route('/catalogue/<int:cat_id>/runtime/<int:runtime>', methods=['GET', 'POST'])
def catalogue_runtime(cat_id, runtime):
	if cat_id is not None:
		#Get the catalogue from id in url
		catalogue = get_catalogue_from_db(cat_id)
		#if this catalogue doesnt exist - 404
		if not catalogue:
			abort(404)
		else:
			movies = get_movies_from_catalogue(cat_id)
			movieResults = []
			for movie in movies:
				if movie['movieruntime'] >= runtime:
					movieResults.append(movie)

			return render_template('base.html', movies=movieResults, catalogue=catalogue, page="catalougeid")
		abort(404)

#This is called when a form processed for release date to fix issues with getting request.form in the url_for method
@app.route('/catalogue/<int:cat_id>/revenue', methods=['GET', 'POST'])
def catalogue_revenue_fix(cat_id):
	if request.form['revenue'] != "":
		revenue = request.form['revenue']
		catid = cat_id
		#Redirect to proper release date url with param from form
		return redirect(url_for('catalogue_revenue', cat_id=catid, revenue=revenue))

#Returns a list of movies of released in the year specified
@app.route('/catalogue/<int:cat_id>/revenue/<int:revenue>', methods=['GET', 'POST'])
def catalogue_revenue(cat_id, revenue):
	if cat_id is not None:
		#Get the catalogue from id in url
		catalogue = get_catalogue_from_db(cat_id)
		#if this catalogue doesnt exist - 404
		if not catalogue:
			abort(404)
		else:
			movies = get_movies_from_catalogue(cat_id)
			movieResults = []
			for movie in movies:
				if movie['movierevenue'] >= revenue:
					movieResults.append(movie)

			return render_template('base.html', movies=movieResults, catalogue=catalogue, page="catalougeid")
		abort(404)

#Returns view for singular movie information
@app.route('/movie/<int:movie_id>')
def movie_id(movie_id):
	if movie_id is not None:
		movie = get_movie_from_db(movie_id)
		return render_template('base.html', movie=movie, page="movie_sep")
	abort(404)

#Returns search results of movies using search term from the form
@app.route('/movies/search', methods=['GET', 'POST'])
def search_movies():
	cattitle = request.form['cataloguetitle']
	catid = get_catalogue_id_from_title(cattitle)
	if request.form['moviesearchterm'] != "":
		#Fixes http bad request errors due to spaces in search term
		searchterm = request.form['moviesearchterm'].replace(" ", "+")
		data = get_movie_data(searchterm, 1)
		page = 1
		totalpages = data['total_pages']
		movieResults = []
		print request.data
		while page <= totalpages:
			for dataEntry in data['results']: 
				movieid = dataEntry['id']
				#REQUESTING TOO MUCH
				#movie = get_movie_from_tmdb_by_id(movieid, True)
				#Set values that are not returned from search to 0
				movie = Movie(dataEntry['title'], dataEntry['overview'], dataEntry['id'], dataEntry['poster_path'], dataEntry['genre_ids'], 0, dataEntry['release_date'], 0, 0)
				movieResults.append(movie)

			data = get_movie_data(searchterm, page)	
			page += 1
		return render_template('base.html', movieResults=movieResults, catid=catid, page="moviesearch")
	flash('Error - You must input a search term')
	return redirect(url_for('catalogue_id', cat_id=catid))

@app.errorhandler(404)
def page_not_found(e):
	return render_template('404.html'), 404

#Returns catalogue data as a dict
#Should only return one single catalogue
def get_catalogue_from_db(cat_id):
	cur = g.db.execute('select Catalogue.catid, Catalogue.title from Catalogue where Catalogue.catid=?', (cat_id,))
	#return [dict(catid=row[0], cattitle=row[1]) for row in cur.fetchall()]
	row = cur.fetchone()
	if row is not None:
		#Remove " from string at last character
		return dict(catid=row[0], cattitle=row[1])
	return None

#TODO REPLACE WITH NEW METHODS
#Get all movies from catalogue using given catid
def get_movies_from_catalogue(cat_id):
	cur = g.db.execute('select CatalogueMovies.movieid, Movie.title, Movie.overview, Movie.poster, Movie.budget, Movie.genre, Movie.releasedate, Movie.runtime, Movie.revenue from Catalogue JOIN CatalogueMovies ON Catalogue.catid = CatalogueMovies.catid JOIN Movie ON CatalogueMovies.movieid = Movie.movieid where Catalogue.catid=?', (cat_id,))
	return [dict(movieid=row[0], movietitle=row[1], movieoverview=row[2], movieposter=row[3], moviebudget=row[4], moviegenre=row[5], moviereleasedate=row[6], movieruntime=row[7], movierevenue=row[8]) for row in cur.fetchall()]

#Returns all movie data from tmdb 
def get_movie_data(query, page):
	url = 'http://api.themoviedb.org/3/search/movie?query={q}&page={p}{k}'.format(q=query, p=page, k=app.config['TMDB_API_KEY'])
	json_obj = urllib2.urlopen(url)
	data = json.load(json_obj)
	return data

#TODO: ADD ALL TMDB ATTRIBUTES FOR MOVIE OBJECTS
#Adds the movie data passed in via a movie object to the database
def add_movie_data_to_database(movie):
	movieids = get_all_movie_ids()
	if movie.movieid not in movieids:
		g.db.execute('insert into Movie (movieid, title, overview, poster, budget, genre, releasedate, runtime, revenue) values (?,?,?,?,?,?,?,?,?)', (int(movie.movieid), movie.title, movie.overview, movie.poster, movie.budget, movie.genre, movie.releasedate, movie.runtime, movie.revenue))
		g.db.commit()

def get_movie_from_db(movie_id):
	cur = g.db.execute('select movieid, title, overview, poster, budget, genre, releasedate, runtime, revenue from movie where movieid=?', (movie_id,))
	x = [dict(movieid=row[0], movietitle=row[1], movieoverview=row[2], movieposter=row[3], moviebudget=row[4], moviegenre=row[5], moviereleasedate=row[6], movieruntime=row[7], movierevenue=row[8]) for row in cur.fetchall()]
	return Movie(x[0]['movietitle'], x[0]['movieoverview'], x[0]['movieid'], x[0]['movieposter'], x[0]['moviebudget'], x[0]['moviegenre'], x[0]['moviereleasedate'], x[0]['movieruntime'], x[0]['movierevenue'])

#Returns the id of the current catalogue using the title of the catalogue
#Used as a workaround to get the cat id in the search movie results page so movies can be added to the catalogue
def get_catalogue_id_from_title(title):
	if title != "":
		cur = g.db.execute('select catid from Catalogue Where Catalogue.title=?', (title,))
		row = cur.fetchone()
		if row != None:
			return row[0]
	return None

#Returns a list of all movie ids that are in the database to stop duplicated data when more than one catalogue has the same movie
def get_all_movie_ids():
	cur = g.db.execute('select movieid from Movie')
	movieids = []
	for row in cur.fetchall():
		if row[0] not in movieids:
			movieids.append(row[0])
	return movieids

#Returns a movie object filled with data from tmdb api using the movie id that was passed in
def get_movie_from_tmdb_by_id(movieid, isSearch):
	url = 'http://api.themoviedb.org/3/movie/{i}?api_key=2cd0314a6c4696283384100d6264c630'.format(i=str(movieid))
	json_obj = urllib2.urlopen(url)
	data = json.load(json_obj)
	movie = Movie(data['title'], data['overview'], data['id'], data['poster_path'], data['budget'], data['genres'][0]['name'], data['release_date'], data['runtime'], data['revenue'])
	return movie


