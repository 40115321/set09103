drop table if exists users;
create table users (
  username text unique not null,
  upassword text not null,
  email text primary key
);


drop table if exists movie;
create table movie ( 
  movieid integer primary key,
  title text not null,
  overview text,
  poster text,
  budget integer, 
  genre text,
  releasedate text,
  runtime integer,
  revenue integer
);

drop table if exists catalogue;
create table catalogue (
  catid integer primary key autoincrement,
  title text not null,
  description text

);

drop table if exists cataloguemovies;
create table cataloguemovies (
	catid integer,
	movieid, integer,
	foreign key(catid) references catalogue(catid),
	foreign key(movieid) references movie(movieid)
);

insert into catalogue(title) values ("Favourite Films");
insert into catalogue(title) values ("Comedys");
insert into catalogue(title) values ("Freaky Fridays");
insert into catalogue(title) values ("Christmas Movies");
insert into catalogue(title) values ("Jackie Chan Flix");
