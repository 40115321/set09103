class Movie(object):
	title = ""
	overview = ""
	movieid = ""
	poster = ""
	budget = ""
	genre = ""
	releasedate = ""
	runtime = ""
	revenue = ""

	def __init__(self, title, overview, movieid, poster, budget, genre, releasedate, runtime, revenue):
		self.title = title
		self.overview = overview
		self.movieid = movieid
		self.poster = poster
		self.budget = budget
		self.genre = genre
		self.releasedate = releasedate
		self.runtime = runtime
		self.revenue = revenue
